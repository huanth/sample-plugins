package jp.co.kodnet.plugins.sample;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.opensymphony.xwork.ActionContext;
import java.util.ArrayList;
import java.util.List;

public class OutputFormAction extends ConfluenceActionSupport {

    private String uname;
    private String sexValue;
    private List<String> interestsValue;
    private final String[] INTERESTSTRING = {"Baseball", "Football", "Rugby", "Sumo", "Tenis", "Table Tennis"};
    private final String[] SEXSTRING = {"Male", "Female"};

    @Override
    public String execute() throws Exception {
        // get parameter in Context from Velocity template
        ActionContext context = ActionContext.getContext();
        uname = this.getParameterValue(context, "uname");
        if (uname == null || "".equals(uname)) {
            return ERROR;
        }
        int sex = Integer.parseInt(this.getParameterValue(context, "sex"));
        String[] interests = getParameterValues(context, "interests");
        // assign parameterValue -> variable of Class
        sexValue = SEXSTRING[sex];
        interestsValue = new ArrayList<String>();
        if (interests != null) {
            for (String interest : interests) {
                interestsValue.add(INTERESTSTRING[Integer.parseInt(interest)]);
            }
        }
        return SUCCESS;
    }

    private String getParameterValue(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }

    private String[] getParameterValues(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return (String[]) paramValues;
        }
        return new String[0];
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getSexValue() {
        return sexValue;
    }

    public void setSexValue(String sexValue) {
        this.sexValue = sexValue;
    }

    public List<String> getInterestsValue() {
        return interestsValue;
    }

    public void setInterestsValue(List<String> interestsValue) {
        this.interestsValue = interestsValue;
    }

}
