/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jp.co.kodnet.plugins.sample;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;

import com.opensymphony.xwork.ActionContext;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author huanth
 */
public class SaveDataAction extends ConfluenceActionSupport implements Beanable{

    private String uname;
    private String sexValue;
    private String resultStr;
    private boolean result;
    //    private List<String> interestsValue;
    private String[] interestsValue;
    private final String[] INTERESTSTRING = {"Baseball", "Football", "Rugby", "Sumo", "Tenis", "Table Tennis"};
    private final String[] SEXSTRING = {"Male", "Female"};

    @Override
    public Object getBean() {
        Map<String, Object> bean = new HashMap<String, Object>();
        //bean.put("result", true);
        //bean.put("resultMessage", this.resultStr);
        bean.put("uname",getUname());
        bean.put("sex",getSexValue());
        bean.put("interestings",getList());
        return bean;
    }
    
    private List<String> getList(){
        List<String> list = Arrays.asList(getInterestsValue());
        return list;
    }
    
    public String toJson() {
        // get parameter in Context from Velocity template
        ActionContext context = ActionContext.getContext();
        uname = this.getParameterValue(context, "uname");
        if (uname == null || "".equals(uname)) {
            return ERROR;
        }
        int sex = Integer.parseInt(this.getParameterValue(context, "sex"));
        String[] interests = getParameterValues(context, "interestings");
        // assign parameterValue -> variable of Class
        sexValue = SEXSTRING[sex];
        interestsValue = new String[interests.length];
        if (interests.length != 0) {
            for (int i = 0; i <= interests.length - 1; i++) {
                interestsValue[i] = INTERESTSTRING[Integer.parseInt(interests[i])];
            }
        }
        JSONObject personDetails = new JSONObject();
        personDetails.put("uname", uname);
        personDetails.put("sex", sexValue);
        JSONArray personObjects = new JSONArray();
        for (String interestValue : interestsValue) {
            personObjects.add(interestValue);
        }
        personDetails.put("interestings", personObjects);
        JSONObject personObject = new JSONObject();
        personObject.put("person", personDetails);
        try {
            Files.write(Paths.get("./store.json"), personObject.toJSONString().getBytes());
        } catch (IOException e) {
            this.result = false;
            this.resultStr = e.getMessage();
            System.out.println("ERROR here: "+e);
        }
        
        this.result = true;
        this.resultStr = "Preocess complete";
        return SUCCESS;
    }

    private String getParameterValue(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }

    private String[] getParameterValues(ActionContext context, String key) {
        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return (String[]) paramValues;
        }
        return new String[0];
    }

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getSexValue() {
        return sexValue;
    }

    public void setSexValue(String sexValue) {
        this.sexValue = sexValue;
    }

    public String[] getInterestsValue() {
        return interestsValue;
    }

    public void setInterestsValue(String[] interestsValue) {
        this.interestsValue = interestsValue;
    }

    public String getResultStr() {
        return resultStr;
    }

    public void setResultStr(String resultStr) {
        this.resultStr = resultStr;
    }  

    /**
     * @return the result
     */
    public boolean isResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(boolean result) {
        this.result = result;
    }

}
