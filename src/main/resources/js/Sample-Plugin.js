/* global AJS */
AJS.toInit(function () {

    $("#ajax-add-button").click(function () {
        var interesting = ["Baseball", "Football", "Rugby", "Sumo", "Tennis", "Table Tennis"];
        var sexs = ["Male", "Female"];
        var interests = [];
        $.each($("input[name='interests']:checked"), function () {
            interests.push(interesting[$(this).val()]);
        });
        var uname = $("#input-one").val();
        var sex = sexs[$("input[name='sex']:checked").val()];

        var row = "<tr><td>+" + uname + "</td><td>+" + sex + "</td><td>+" + interests.join() + "</td></tr>";
        $("#addTable").append(row).removeClass("hidden");

        /*        $.ajax({
         type: 'POST',
         url: '/echo/json/',
         dataType: "json",
         data: //JSON.stringify(order),
         {
         json: JSON.stringify({
         name: uname,
         sex: sex,
         inter: interesting
         }), delay: 1
         },
         contentType: "application/json; charset=utf-8",
         success: function (data) {
         
         },
         fail: function () {
         alert('problem');
         }
         });
         */
    });

    $("#ajax-url-button").click(function () {
        var interests = [];
        $.each($("input[name='interests']:checked"), function () {
            interests.push($(this).val());
        });
        var uname = $("#input-one").val();
        var sex = $("input[name='sex']:checked").val();
        alert(uname + " " + sex + " " + interests);

        $.ajax({
            async: true,
            type: 'GET',
            url: AJS.contextPath() + "/plugins/questionary/toJson.action",
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            data: {
                uname: uname,
                sex: sex,
                interestings: interests
            },
            success: function (returnDataRerult) { 
              
                var event_data = '';

                /*console.log(value);*/
                event_data += '<tr>';
                event_data += '<td>' + returnDataRerult.uname + '</td>';
                event_data += '<td>' + returnDataRerult.sex + '</td>';
                event_data += '<td>';
                var myArray =  $.makeArray(returnDataRerult.interestings);
                alert(myArray.length);
                $.each(myArray,function(index,value){
                    event_data+='<span>'+value+'</span> ';
                });
                event_data+='</td>';
                event_data += '</tr>';
                $("#addTable").append(event_data);
            },
            done:function (result){
                alert(result);
            },
            error: function (data) {
                /*console.log("error");*/
                alert("404. Please wait until the File is Loaded.");
            }
        });

    });

    AJS.$("#warning-dialog-show-button").click(function () {
        var field = $("#input-one");
        if (field.val() === "") {
            if ($("#a-custom-context").find("div").length === 0) {
                AJS.messages.warning("#a-custom-context", {
                    title: 'Warnning!!!',
                    body: '<p> Name not blank.</p>'
                });
                $("h1").css("color", "red");
            }
        } else {
            AJS.dialog2("#demo-dialog").show();
        }
    });

    AJS.$("#json-add-button").click(function () {
        $("#valid-submit-form").submit();
    });

    AJS.$("#dialog-submit-button").click(function () {
        $("#valid-submit-form").submit();
    });

    AJS.$("#show-dialog-button").click(function () {
        AJS.dialog2("#demo-dialog").show();
    });

    AJS.$("#dialog-cancel").click(function () {
        AJS.dialog2("#demo-dialog").hide();
    });
    /*AJS.$('#valid-submit-form').on('aui-valid-submit', function (event) {
     console.log('Data saved');
     event.preventDefault();
     return;
     });*/

    AJS.$("#warning-dialog-show-button").on('click', function (e) {
        /*<!--e.preventDefault();-->*/
        AJS.dialog2("#demo-warning-dialog").show();
    });
    AJS.$(document).on("click", "#demo-warning-dialog button", function (e) {
        /* <!--e.preventDefault();-->*/
        AJS.dialog2("#demo-warning-dialog").hide();
    });
});
/*function showWarnning() {
 AJS.messages.warning("#a-custom-context", {
 title: 'Warnning!!!',
 body: '<p> Name not blank.</p>'
 });
 }*/


