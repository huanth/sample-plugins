package ut.jp.co.family.sample.plugin;

import org.junit.Test;
import jp.co.family.sample.plugin.MyPluginComponent;
import jp.co.family.sample.plugin.MyPluginComponentImpl;

import static org.junit.Assert.assertEquals;

public class MyComponentUnitTest
{
    @Test
    public void testMyName()
    {
        MyPluginComponent component = new MyPluginComponentImpl(null);
        assertEquals("names do not match!", "myComponent",component.getName());
    }
}